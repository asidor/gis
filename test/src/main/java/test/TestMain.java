package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import gis.algorithms.Algorithm;
import gis.algorithms.impl.BoruvkaAlgorithm;
import gis.algorithms.impl.KruskalAlgorithm;
import gis.algorithms.impl.PrimAlgorithm;
import gis.common.Edge;
import gis.common.Graph;
import gis.generators.GraphGenerator;

public class TestMain {

	public static void main(String args[]) throws IOException {
		GraphGenerator generator = new GraphGenerator();
		int v = 20;
		int maxE =  v * (v - 1) / 2;
		int minE = v-1;
/*		for(int i = minE; i<maxE; i++) {
			Graph graph = generator.generateGraph(v, i, 0, 100);
			graph.toFile("graph" + i + ".txt");
			System.out.println(i);
		}*/
		System.out.println("finish");
		//Test1 test1 = new Test1(minE, maxE);
		for(int i = 20; i<maxE; i++) {
			File file = new File("graph" + i + ".txt");
			Graph graph = new Graph(file);
			Algorithm alg = new BoruvkaAlgorithm();
			Graph out = alg.execute(graph);
			if(i == 20) {
				System.out.println(out.toString());
				int sum = 0;
				for(Edge edge : out.edges) {
					sum +=edge.weight;
				}
				System.out.println("waga: " + sum);
			}
			System.out.println(i);
		}
		//Graph graph = generator.generateGraph(v, maxE, 0, 100);		
		
	}

}
