package test;

import gis.algorithms.Algorithm;
import gis.algorithms.impl.BoruvkaAlgorithm;
import gis.algorithms.impl.KruskalAlgorithm;
import gis.algorithms.impl.PrimAlgorithm;
import gis.common.Graph;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Test1 {

	public Test1(int minE, int maxE) throws IOException {
		File f = new File("..\\wyniki3.txt");
		FileWriter writer = null;
		StringBuilder sb = new StringBuilder();
		try {
			writer = new FileWriter(f);
			//StringBuilder sb = new StringBuilder();
			for(int i = minE; i<maxE; i++) {
				File file = new File("graph" + i + ".txt");
				Graph inputGraph = new Graph(file);
				//Algorithm alg = new KruskalAlgorithm();
				Algorithm alg = new BoruvkaAlgorithm();
				Date startTime = new Date();
				try{
					alg.execute(inputGraph);
				} catch (java.lang.NullPointerException r) {
					continue;
				}
				//alg.execute(inputGraph);
				Date endTime = new Date();
				long time = endTime.getTime()-startTime.getTime();
				sb.append(inputGraph.getV() + ";" + i + ";" + time + "\n");
				System.out.println(i);
		}
		
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(java.lang.NullPointerException e) {
			
		}finally {
			writer.write(sb.toString());
			writer.close();
		}
	}
}
