package gis.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

/**
 * Klasa implementuj�ca graf
 * @author adr
 *
 */
public class Graph {
	/**
	 * lista kraw�dzi
	 */
	public List<Edge> edges = new ArrayList<Edge>();
	
	private int change = 0;
	/**
	 * liczba wierzcho�k�w
	 */
	private int V;
	/**
	 * lista wierzcho�k�w
	 */
	private List<String> nodes;
	/**
	 * mapa opisuj�ca liste s�siad�w dla ka�dego wierzcho�ka
	 */
	private Map<String, List<String>> adj = new HashMap<String, List<String>>();
	
	public Graph(File file) throws IOException {
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		List<String> nodes = new ArrayList<String>();
		while( (line = br.readLine()) != null) {
			String[] args = line.split(":");
			Edge edge = new Edge(args);
			edges.add(edge);
			updateAdjacents(edge, true);
			for(int i = 0; i<2; i++) {
				if(!nodes.contains(args[i])) {
					nodes.add(args[i]);
				}
			}
		}
		this.V = nodes.size();
		this.nodes = nodes;
		br.close();
		fr.close();
	}
	
	public Graph() {
		nodes = new ArrayList<String>();
		this.V = 0;
	}
	/**
	 * Metoda zapisuje graf do pliku
	 * @param filename
	 * @throws IOException
	 */
	public void toFile(String filename) throws IOException {
		StringBuilder builder = new StringBuilder();
		for(Edge edge : edges) {
			builder.append(edge.firstNode + ":");
			builder.append(edge.secondNode + ":");
			builder.append(edge.weight);
			builder.append("\n");
		}
		File file = new File(filename);
		FileWriter writer = new FileWriter(file);
		writer.write(builder.toString());
		writer.close();
	}
	/**
	 * metoda dodaje wierzcho�ek do grafu
	 * @param node
	 */
	public void addNode(String node) {
		nodes.add(node);
	}
	/**
	 * metoda zwraca list� wierzcho�k�w grafu
	 * @return
	 */
	public List<String> getNodes() {
		return nodes;
	}
	/**
	 * metoda dodaje kraw�d� do grafu
	 * @param edge
	 */
	public void addEdge(Edge edge) {
		edges.add(edge);
		updateAdjacents(edge, true);
		this.change = 0;
		if(!nodes.contains(edge.firstNode)) {
			nodes.add(edge.firstNode);
			this.change++;
		}
		if(!nodes.contains(edge.secondNode)) {
			nodes.add(edge.secondNode);
			this.change++;
		}
		this.V = nodes.size();
	}
	/**
	 * metoda usuwa kraw�d� z grafu
	 * @param edge
	 */
	public void removeEdge(Edge edge) {
		edges.remove(edge);
		updateAdjacents(edge, false);
		for(int i = 0; i<this.change; i++) {
			nodes.remove(V-1);
			V--;
		}
	}
	/**
	 * metoda zwraca liczb� wierzcho�k�w w grafie
	 * @return
	 */
	public int getV() {
		return V;
	}
	/**
	 * metoda uaktualnia map� s�siad�w wierzcho�k�w
	 * @param edge
	 * @param add
	 */
	public void updateAdjacents(Edge edge, boolean add) {
		if(add) {
			addAdjacent(edge.firstNode, edge.secondNode);
			addAdjacent(edge.secondNode, edge.firstNode);
		} else {
			removeAdjacent(edge.firstNode, edge.secondNode);
			removeAdjacent(edge.secondNode, edge.firstNode);
		}

		
	}
	/**
	 * metoda usuwa s�siada wierzcho�ka
	 * @param node1
	 * @param node2
	 */
	public void removeAdjacent(String node1, String node2) {
		if(adj.containsKey(node1)) {
			List<String> neighbours = adj.get(node1);
			neighbours.remove(node2);
		}
	}
	/**
	 * metoda dodaje s�siada wierzcho�ka
	 * @param node1
	 * @param node2
	 */
	public void addAdjacent(String node1, String node2) {
		if(!adj.containsKey(node1)) {
			List<String> neighbours = new ArrayList<String>();
			neighbours.add(node2);
			adj.put(node1, neighbours);
		} else {
			List<String> neighbours = adj.get(node1);
			if(!neighbours.contains(node2)) {
				neighbours.add(node2);
			}
		}
	}
	
	public boolean equals(Object other) {
		if(other == null)
			return false;
		if(this == other)
			return true;
		if(!(other instanceof Graph))
			return false;
		
		Graph graph = (Graph)other;
		if(edges.size() != graph.edges.size())
			return false;
		
		Set<Edge> edges1 = new HashSet<Edge>();
		edges1.addAll(edges);
		Set<Edge> edges2 = new HashSet<Edge>();
		edges2.addAll(graph.edges);
		if(edges1.equals(edges2))
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		//liczba wierzcho�k�w
		sb.append("Number of nodes: " + V + "\n");
		//lista wierzcho�k�w
		sb.append("Nodes:");
		for(String node : nodes) {
			sb.append(" " + node);
		}
		sb.append("\n");
		//lista kraw�dzi
		sb.append("Edges:" + "\n");
		for(Edge edge : edges) {
			sb.append(edge.firstNode);
			sb.append(" ");
			sb.append(edge.secondNode);
			sb.append(" ");
			sb.append(edge.weight);
			sb.append("\n");
		}
		//lista s�siad�w dla wszystkich w�z��w
		sb.append("Adjacents:" + "\n");
		for(Entry<String, List<String>> entry : adj.entrySet()) {
			sb.append(entry.getKey() + ":");
			for(String adjacent : entry.getValue()) {
				sb.append(" " + adjacent);
			}
			sb.append("\n");
		}
		
		return sb.toString();		
	}
	/**
	 * pomocnicza metoda do sprawdzania czy graf ma cykl
	 * @param v
	 * @param visited
	 * @param parent
	 * @return
	 */
	private boolean isCyclicUtil(String v, Map<String, Boolean> visited, String parent) {
		//oznacz wierzcholek jako odwiedzony
		visited.put(v, true);
		
		//powtarzaj dla wszystkich wierzcho�k�w s�siaduj�cych z tym wierzcho�kiem
		List<String> adjacentNodes = adj.get(v);
		for(String node : adjacentNodes) {
			//jesli s�siad nie zostal odwiedzony to powtorz dla tego s�siada
			if(!visited.get(node)) {
				if(isCyclicUtil(node, visited, v))
					return true;
			}
			//jesli sasiad jest odwiedzony a nie jest to rodzic obecnego wierzcho�ka to jest cykl
			else if(!node.equals(parent))
				return true;
		}
		
		return false;
	}
	/**
	 * metoda do sprawdzania czy graf ma cykl
	 * @return
	 */
	public boolean hasCycle() {
		Map<String, Boolean> visited = new HashMap<String, Boolean>(V);
		for(String node : nodes) {
			visited.put(node, false);
		}

		for(String node : nodes) {
			if (!visited.get(node))
				if (isCyclicUtil(node, visited, "-1"))
					return true;
		}
		
		return false;
	}

}
