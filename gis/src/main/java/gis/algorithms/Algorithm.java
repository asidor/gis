package gis.algorithms;

import gis.common.Graph;

/**
 * Interfejs Algorytmu
 * @author adr
 */
public interface Algorithm {

	/**
	 * Generuje graf wed�ug algorytmu na podstawie wej�ciowego grafu
	 * @param inputGraph graf wej�ciowy
	 * @return graf wygenerowany przez algorytm
	 */
	public Graph execute(Graph inputGraph);
}
