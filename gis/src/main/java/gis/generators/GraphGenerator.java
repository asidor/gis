package gis.generators;

import gis.common.Edge;
import gis.common.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generuje losowy graf o zadanej liczbie wierzcho�k�w oraz kraw�dzi losowymi wagami
 * 
 * @author adr
 */
public class GraphGenerator {

	/**
	 * 
	 * @param v
	 *            liczba wierzcho�k�w
	 * @param e
	 *            liczba kraw�dzi
	 * @param minWeight
	 *            minimalna mo�liwa waga kraw�dzi
	 * @param maxWeight
	 *            maksymalna mo�liwa waga kraw�dzi
	 * @return losowo wygenerowany graf
	 */
	public Graph generateGraph(int v, int e, int minWeight, int maxWeight) {
		if (e > v * (v - 1) / 2)
			throw new IllegalArgumentException("Too many edges");
		if (e < v - 1)
			throw new IllegalArgumentException("Too few edges");

		Random random = new Random();

		Graph graph = new Graph();

		List<Integer> nodes = new ArrayList<Integer>();
		for (int i = 0; i < v; i++) {
			nodes.add(i);
		}
		// int k = 0;
		while (graph.edges.size() < (v - 1)) {
			int node1 = random.nextInt(v);
			int node2;
			if (nodes.size() != 0) {
				int index = random.nextInt(nodes.size());
				node2 = nodes.get(index);
			} else {
				node2 = random.nextInt(v);
			}
			if (node1 == node2) {
				continue;
			}

			int weight = random.nextInt((maxWeight - minWeight) + 1) + minWeight;
			Edge edge = new Edge(node1, node2, weight);

			if (nodes.size() != 0) {
				graph.addEdge(edge);
				nodes.remove(Integer.valueOf(node1));
				nodes.remove(Integer.valueOf(node2));
			} else {
				if (!graph.edges.contains(edge)) {
					graph.addEdge(edge);
				}
				if (graph.hasCycle()) {
					graph.removeEdge(edge);
					continue;
				}
			}
			// k++;
		}

		while (graph.edges.size() != e) {
			int node1 = random.nextInt(v);
			int node2 = random.nextInt(v);
			if (node1 == node2)
				continue;

			int weight = random.nextInt((maxWeight - minWeight) + 1)
					+ minWeight;
			Edge edge = new Edge(node1, node2, weight);

			if (!graph.edges.contains(edge)) {
				graph.addEdge(edge);
			}
		}

		return graph;

	}
}
