package gis.generators;

import gis.common.Edge;
import gis.common.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generuje losowy graf o zadanej liczbie kraw�dzi z losowymi wagami o podanej maksymalnej liczbie wierzcho�k�w
 * @author adr
 */
public class GraphGenerator {

	/**
	 * 
	 * @param v maksymalna liczba wierzcho�k�w
	 * @param e liczba kraw�dzi
	 * @param minWeight minimalna mo�liwa waga kraw�dzi
	 * @param maxWeight maksymalna mo�liwa waga kraw�dzi
	 * @return losowo wygenerowany graf
	 */
	public Graph generateGraph(int v, int e, int minWeight, int maxWeight) {
		if (e > v * (v - 1) / 2)
			throw new IllegalArgumentException("Too many edges");
		if (e < 0)
			throw new IllegalArgumentException("Too few edges");

		Random random = new Random();

		Graph graph = new Graph();
		while (graph.edges.size() != e) {
			int node1 = random.nextInt(v);
			int node2 = random.nextInt(v);
			if(node1 == node2)
				continue;
			
			int weight = random.nextInt((maxWeight - minWeight) + 1) + minWeight;
			Edge edge = new Edge(node1, node2, weight);
			
			if(!graph.edges.contains(edge)) {
				graph.addEdge(edge);
			}
		}
		
		return graph;

	}
}
