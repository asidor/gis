package gis.algorithms.impl;

import java.util.Collections;

import gis.algorithms.Algorithm;
import gis.common.Edge;
import gis.common.EdgeWeightComparator;
import gis.common.Graph;

/**
 * Klasa implementująca algorytm Kruskala do znajdowania najmniejszego dzewa rozpinającego
 * @author adr
 *
 */
public class KruskalAlgorithm implements Algorithm{

	/**
	 * Wykonuje algorytm Kruskala
	 */
	public Graph execute(Graph inputGraph) {
		Collections.sort(inputGraph.edges, new EdgeWeightComparator());
		Graph outputGraph = new Graph();
		//check if output graph has all nodes
		for(Edge edge : inputGraph.edges) {
			if(outputGraph.getV() != inputGraph.getV()) {
				outputGraph.addEdge(edge);
				if(outputGraph.hasCycle()) {
					outputGraph.removeEdge(edge);
				}
			} else {
				break;
			}
		}

		return outputGraph;
	}

	
}
