package gis.algorithms.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gis.algorithms.Algorithm;
import gis.common.Edge;
import gis.common.EdgeWeightComparator;
import gis.common.Graph;

/**
 * Klasa implementująca algorytm Prima do znajdowania najmniejszego drzewa rozpinającego
 * @author adr
 *
 */
public class PrimAlgorithm implements Algorithm{

	public Graph execute(Graph inputGraph) {
		//choosing starting node
		List<String> inputNodes = inputGraph.getNodes();
		String newNode = inputGraph.getNodes().get(0);
		//all edges from input graph
		List<Edge> allEdges = new ArrayList<Edge>(inputGraph.edges);
		//adjacent edges to choose from
		List<Edge> adjacentEdges = new ArrayList<Edge>();
		
		Graph outputGraph = new Graph();	
		//List<String> outputNodes = outputGraph.getNodes();
		
		while(inputGraph.getNodes().size() != outputGraph.getNodes().size()) {
		adjacentEdges.addAll(getAdjacentEdges(allEdges, newNode));
		Collections.sort(adjacentEdges, new EdgeWeightComparator());

		List<Edge> redundantEdges = new ArrayList<Edge>();
		for(Edge edge : adjacentEdges) {
			outputGraph.addEdge(edge);
			redundantEdges.add(edge);
			if(outputGraph.hasCycle()) {
				outputGraph.removeEdge(edge);
				redundantEdges.add(edge);
				//adjacentEdges.remove(edge);
			} else {
				//adjacentEdges.remove(edge);
				newNode = outputGraph.getNodes().get(outputGraph.getV()-1);
				break;
			}
			/*if(!outputNodes.contains(edge.firstNode) || !outputNodes.contains(edge.secondNode)) {
				outputGraph.addEdge(edge);
				newNode = outputNodes.get(outputNodes.size()-1);
				adjacentEdges.remove(edge);
				break;
			}*/
		}
		adjacentEdges.removeAll(redundantEdges);
		}
		
		return outputGraph;
	}
	
	/**
	 * Wyszukuje przyległe krawędzie do danego wierzchołka
	 * @param edges
	 * @param node
	 * @return
	 */
	private List<Edge> getAdjacentEdges(List<Edge> edges, String node) {
		List<Edge> adjacentEdges = new ArrayList<Edge>();
		for(Edge edge : edges) {
			if(edge.contains(node)) {
				adjacentEdges.add(edge);
			}
		}
		edges.removeAll(adjacentEdges);
		
		return adjacentEdges;
	}
	

}
