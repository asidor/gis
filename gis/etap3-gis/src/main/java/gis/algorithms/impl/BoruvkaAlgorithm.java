package gis.algorithms.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gis.algorithms.Algorithm;
import gis.common.Edge;
import gis.common.EdgeWeightComparator;
import gis.common.Graph;

/**
 * Klasa implementuj�ca algorytm Boruvki znajduj�cy najmniejsze drzewo rozpinaj�ce
 * @author adr
 *
 */
public class BoruvkaAlgorithm implements Algorithm {

	/**
	 * 
	 */
	public Graph execute(Graph inputGraph) {
		List<Edge> edges = new ArrayList<Edge>(inputGraph.edges);
		for(int i = 0; i<edges.size(); i++) {
			edges.get(i).index = i;
		}
		// utworz grafy o jednym wierzcholku i zero krawedziach
		List<Graph> graphs = new ArrayList<Graph>();
		for (String node : inputGraph.getNodes()) {
			Graph graph = new Graph();
			graph.addNode(node);
			graphs.add(graph);
		}

		while (graphs.size() != 1) {
			// dla kazdego grafu znajdz i dodaj nowa najlzejsza krawedz
			Set<Edge> addedEdges = new HashSet<Edge>();
			for (Graph graph : graphs) {
				List<Edge> adjacentEdges = new ArrayList<Edge>();
				for (String node : graph.getNodes()) {
					// sprawdzic czy dodaja sie duplikaty
					adjacentEdges.addAll(getAdjacentEdges(edges, node));

				}
				// sposrod wszystkich krawedzi wybierz najlzejsza
				while (true) {
					Edge lightestEdge = findLightestEdge(adjacentEdges);
					graph.addEdge(lightestEdge);
					if (graph.hasCycle()) {
						graph.removeEdge(lightestEdge);
						adjacentEdges.remove(lightestEdge);
						continue;
					}
					addedEdges.add(lightestEdge);
					break;
				}
			}
			// usun wszystkie dodane krawedzie (nie beda juz potrzebne)
			edges.removeAll(addedEdges);
			// polacz grafy
			graphs = uniteGraphs(graphs);
		}

		return graphs.get(0);
	}

	/**
	 * Metoda scalaj�ca grafy
	 * @param graphs
	 * @return
	 */
	private List<Graph> uniteGraphs(List<Graph> graphs) {
		List<Graph> outputGraphs = new ArrayList<Graph>();
		int k = 0;
		boolean united;
		while (k < graphs.size()) {
			united = false;
			Graph mainGraph = graphs.get(k);
			List<Graph> removeGraphs = new ArrayList<Graph>();
			List<Edge> addEdges = new ArrayList<Edge>();
			for (int i = k + 1; i < graphs.size(); i++) {
				Graph g = graphs.get(i);
				if (mainGraph.equals(g)) {
					removeGraphs.add(g);
					//graphs.remove(g);
				} else if (shareNode(mainGraph, g)) {
					//polacz grafy
					// sprawdz czy nie dodaje duplikatow (chyba jednak nie moze)
					for (Edge edge : g.edges) {
						if(!addEdges.contains(edge) && !mainGraph.edges.contains(edge)) {
							addEdges.add(edge);
							//mainGraph.addEdge(edge);
						}
					}
					removeGraphs.add(g);
					united = true;
					//graphs.remove(g);
				}
			}
			graphs.removeAll(removeGraphs);
			for(Edge edge : addEdges) {
				mainGraph.addEdge(edge);
			}
			if(!united) {
				outputGraphs.add(mainGraph);
				graphs.remove(mainGraph);
			}
			
			//k++;
		}
		
		return outputGraphs;

	}

	/**
	 * Sprawdza czy grafy dziel� wsp�lny wierzcho�ek
	 * @param g1
	 * @param g2
	 * @return
	 */
	private boolean shareNode(Graph g1, Graph g2) {
		for (String node : g1.getNodes()) {
			List<String> nodes = g2.getNodes();
			if (nodes.contains(node))
				return true;
		}

		return false;
	}

	/**
	 * Wyszukuje przyleg�e kraw�dzie do danego wierzcho�ka
	 * @param edges
	 * @param node
	 * @return
	 */
	private List<Edge> getAdjacentEdges(List<Edge> edges, String node) {
		List<Edge> adjacentEdges = new ArrayList<Edge>();
		for (Edge edge : edges) {
			if (edge.contains(node)) {
				adjacentEdges.add(edge);
			}
		}

		return adjacentEdges;
	}

	/**
	 * Wybiera kraw�d� z najmniejsz� wag�
	 * @param edges
	 * @return
	 */
	private Edge findLightestEdge(List<Edge> edges) {
		Edge lightestEdge = null;
		EdgeWeightComparator comparator = new EdgeWeightComparator();
		for (int i = 0; i < edges.size(); i++) {
			if (i == 0) {
				lightestEdge = edges.get(i);
			} else {
				if (comparator.compare(lightestEdge, edges.get(i)) > 0) {
					lightestEdge = edges.get(i);
				} else if(comparator.compare(lightestEdge, edges.get(i)) == 0) {
					if(lightestEdge.index > edges.get(i).index)
						lightestEdge = edges.get(i);
				}
			}
		}

		return lightestEdge;
	}

}
