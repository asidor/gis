package gis.common;

import java.util.Comparator;
/**
 * Klasa implementuj�ca Comparator do por�wnywania kt�ra kraw�d� ma mniejsz� wag�
 * @author adr
 *
 */
public class EdgeWeightComparator implements Comparator<Edge>{

	public int compare(Edge edge1, Edge edge2) {
		return edge1.weight - edge2.weight;
	}

}
