package gis.common;

import gis.algorithms.Algorithm;
import gis.algorithms.impl.BoruvkaAlgorithm;
import gis.algorithms.impl.KruskalAlgorithm;
import gis.algorithms.impl.PrimAlgorithm;
import gis.generators.GraphGenerator;

import java.io.File;
import java.io.IOException;

public class Main {
	private static final String GENERATE = "generate";
	
	private static final String MST = "mst";
	
	private static final String KRUSKAL = "kruskal";
	
	private static final String PRIM = "prim";
	
	private static final String BORUVKA = "boruvka";

	public static void main(String[] args) throws IOException {
		String command = args[0];
		if(command.equals(GENERATE)) {
			if(args.length == 5) {
				int v = Integer.valueOf(args[1]);
				int e = Integer.valueOf(args[2]);
				int min = Integer.valueOf(args[3]);
				int max = Integer.valueOf(args[4]);
				GraphGenerator generator = new GraphGenerator();
				Graph graph = generator.generateGraph(v,e,min,max);
				graph.toFile("output.txt");
			}
		} else if(command.equals(MST)) {
			if(args.length == 3) {
				String alg = args[1];
				String fileName = args[2];
				File file = new File(fileName);
				Algorithm algorithm = null;
				if(alg.equals(KRUSKAL)) {				
					algorithm = new KruskalAlgorithm();
				} else if(alg.equals(PRIM)) {
					algorithm = new PrimAlgorithm();
				} else if(alg.equals(BORUVKA)) {
					algorithm = new BoruvkaAlgorithm();
				} else {
					System.out.println("No such algorithm");
					System.exit(0);
				}
				
				Graph inputGraph = new Graph(file);
				Graph outputGraph = algorithm.execute(inputGraph);
				System.out.println(outputGraph.toString());
				outputGraph.toFile("mst.txt");
			}
		}

	}
}
