package gis.common;

/**
 * Klasa implementuj�ca kraw�d�
 * @author adr
 *
 */
public class Edge {
	/**
	 * Pierwszy wierzcho�ek
	 */
	public String firstNode;
	/**
	 * drugi wierzcho�ek
	 */
	public String secondNode;
	/**
	 * waga kraw�dzi
	 */
	public int weight;
	/**
	 * indeks okre�laj�cy kt�ra kraw�d� jest pierwsza w przypadku kraw�dzi o tych samych wagach
	 */
	public int index;

	public Edge(String[] args) {
		firstNode = args[0];
		secondNode = args[1];
		weight = Integer.valueOf(args[2]);
	}
	
	public Edge(int node1, int node2, int weight) {
		firstNode = String.valueOf(node1);
		secondNode = String.valueOf(node2);
		this.weight = weight;
	}
	
	public boolean contains(String node) {
		if(node.equals(firstNode) || node.equals(secondNode))
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		return firstNode + " " + secondNode + " " + weight;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == null)
			return false;
		if(this == other)
			return true;
		if(!(other instanceof Edge))
			return false;
		
		Edge edge = (Edge)other;
		if(this.contains(edge.firstNode) && this.contains(edge.secondNode))
			return true;
		
		return false;
		
	}
}
